#!/usr/bin/env python

import argparse
import configparser
import asyncio
import discord
import shlex
import re
import time
import random
import tempfile
import uuid

def load_config(fh):
    cfgparser = configparser.ConfigParser()
    cfgparser.read(fh)
    return cfgparser

def get_usagestr(prefix):
    out = '**General commands: (Prefix is {prefix})**\n'.format(prefix=prefix)
    out += '{prefix}summon: Summon the selected AI to play a game on the current channel'.format(prefix=prefix)

    return out

wagonmode = True

def main(config):
    token = config.get('DEFAULT', 'token', fallback=None)
    if token is None: raise ValueError('Invalid token: {}'.format(token))

    allowed_channels = shlex.split(config.get('DEFAULT', 'allowed_channels', fallback=''))

    prefix = config.get('DEFAULT', 'prefix', fallback='/')

    defaultvotetime = config.getint('DEFAULT', 'votetime', fallback=30)

    yesemoji = config.get('DEFAULT', 'yes', fallback='👍')
    noemoji = config.get('DEFAULT', 'no', fallback='👎')

    client = discord.Client()

    games = []

    usagestr = get_usagestr(prefix=prefix)

    @client.event
    async def on_message(message):
        if allowed_channels and (message.channel.name not in allowed_channels): return
        elif not message.content.startswith(prefix): return
        elif message.author == client.user: return

        noprefix = message.content[len(prefix):].lower()
        if False: pass

        ### HELP ###
        elif noprefix.startswith('help'): await message.channel.send(usagestr)

        ### HELP ###
        elif noprefix.startswith('summon'): await message.channel.send('No AIs found in current directory!')

    print('Started!')
    client.run(token)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('config')
    args = parser.parse_args()

    with open(args.config) as fh: config = load_config(args.config)

    main(config)
